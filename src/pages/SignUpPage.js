import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import {
  Button,
  Form,
  Grid,
  Header,
  Message,
  Segment,
} from "semantic-ui-react";
import "../App.css";
import axios from "axios";

const SignUp = ({ setOpenSignUp }) => {
  const [formDetails, setFormDetails] = useState({
    username: "",
    emailId: "",
    password: "",
  });
  //   const history = useHistory();
  async function submitForm() {
    try {
      console.log(formDetails);
      await axios
        .post("http://localhost:3005/signup", formDetails)
        .then((res) => {
          console.log(res);

          window.location.href = "http://localhost:3000/";
        })
        .catch((err) => {
          localStorage.removeItem("daily-contents-token");
          localStorage.removeItem("daily-contents-email");
        });
    } catch (err) {}
    // console.log(formDetails);
    // history.push("/");
  }
  function logIn() {
    setOpenSignUp(true);
  }

  return (
    <>
      <Header
        as="h1"
        color="black"
        textAlign="center"
        style={{ padding: "2vh" }}
      >
        DAILY CONTENT
      </Header>
      <Grid
        textAlign="center"
        style={{ height: "80vh" }}
        verticalAlign="middle"
      >
        <Grid.Column style={{ maxWidth: 450 }}>
          <Header as="h2" color="black" textAlign="center">
            Create a new account
          </Header>
          <Form size="large">
            <Segment stacked>
              <Form.Input
                fluid
                icon="user"
                iconPosition="left"
                placeholder="User Name"
                onChange={(e) =>
                  setFormDetails({ ...formDetails, username: e.target.value })
                }
              />

              <Form.Input
                fluid
                icon="envelope"
                iconPosition="left"
                placeholder="E-mail address"
                onChange={(e) =>
                  setFormDetails({ ...formDetails, emailId: e.target.value })
                }
              />
              <Form.Input
                fluid
                icon="lock"
                iconPosition="left"
                placeholder="Password"
                type="password"
                onChange={(e) =>
                  setFormDetails({ ...formDetails, password: e.target.value })
                }
              />

              <Button color="black" fluid size="large" onClick={submitForm}>
                SignUp
              </Button>
            </Segment>
          </Form>
          <Button color="white" fluid size="large" onClick={logIn}>
            Login
          </Button>
        </Grid.Column>
      </Grid>
    </>
  );
};

export default SignUp;
