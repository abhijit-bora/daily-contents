import React from "react";
import { useState } from "react";
import { useHistory } from "react-router-dom";
import { Card, Button, Image } from "semantic-ui-react";
import day from "../data/day";
import "../App";
import axios from "axios";
import DailyContent from "./DailyContentPage";

function AllContents() {
  const [openContent, setOpenContent] = React.useState(true);
  // const [open, setOpen] = useState(false);
  // const history = useHistory();
  function readmore() {
    setOpenContent(false);
    // history.push(`/DailyContent`);
  }

  async function loggedOut() {
    localStorage.removeItem("daily-contents-token");
    localStorage.removeItem("daily-contents-email");
    window.location.href = "http://localhost:3000";
    // history.push("/");
  }
  let newDate = new Date();
  let date = newDate.getDate();
  let i = newDate.getDay();
  console.log(day.day[i]);
  return (
    <>
      {openContent ? (
        <>
          <div className="navbar">
            <h2 className="ui left floated header">ALL CONTENTS</h2>
            <h2 className="ui right floated header">
              <Button onClick={loggedOut}>Log Out</Button>
            </h2>
          </div>
          <div className="grid-container">
            <Card className="grid-item">
              <Image
                src="https://nypost.com/wp-content/uploads/sites/2/2021/11/astrology-sun-moon-rising-signs-1-copy.jpg?quality=90&strip=all&w=744"
                wrapped
                ui={false}
              />
              <Card.Content>
                <Card.Header>Card Title</Card.Header>
                <Card.Meta>
                  <span className="date">
                    Date:{date} Day:{day.day[i]}
                  </span>
                </Card.Meta>
                <Card.Description>Card description</Card.Description>
                <Card.Content extra>
                  <Button
                    //   onClick={readmore}
                    onClick={() => {
                      readmore();
                    }}
                    className="ui button"
                  >
                    More Content
                  </Button>
                </Card.Content>
              </Card.Content>
            </Card>
          </div>
        </>
      ) : (
        <DailyContent setOpenContent={setOpenContent} />
      )}
    </>
  );
}
export default AllContents;
