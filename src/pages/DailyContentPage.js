import React, { useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import { Button } from "semantic-ui-react";

import "../App.css";
import Slider from "../components/Slider/Slider";

function DailyContent({ setOpenContent }) {
  // const history = useHistory();

  // let { id } = useParams();
  async function loggedOut() {
    localStorage.removeItem("daily-contents-token");
    localStorage.removeItem("daily-contents-email");
    window.location.href = "http://localhost:3000";
    // history.push("/");
  }

  async function back() {
    setOpenContent(true);
  }

  return (
    <>
      <div className="navbar">
        <Button
          labelPosition="left"
          icon="left chevron"
          content="Back"
          onClick={back}
        />

        <h2 className="ui right floated header">
          <Button onClick={loggedOut}>Log Out</Button>
        </h2>
      </div>
      <Slider />
    </>
  );
}

export default DailyContent;
