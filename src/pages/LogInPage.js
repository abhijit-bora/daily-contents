import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import {
  Button,
  Form,
  Grid,
  Header,
  Message,
  Segment,
} from "semantic-ui-react";
import "../App.css";
import axios from "axios";
import SignUp from "./SignUpPage";

const LogInPage = () => {
  const [openSignUp, setOpenSignUp] = useState(true);
  const [formDetails, setFormDetails] = useState({ emailId: "", password: "" });
  // const history = useHistory();
  async function submitForm() {
    try {
      console.log(formDetails);
      await axios
        .post("http://localhost:3005/signin", formDetails)
        .then((res) => {
          console.log(res);
          localStorage.setItem("daily-contents-token", res.data.token);
          localStorage.setItem("daily-contents-email", res.data.user_.emailId);
          localStorage.setItem("number", res.data.user_.dailynumber);
          window.location.href = "http://localhost:3000/";
        })
        .catch((err) => {
          localStorage.removeItem("daily-contents-token");
          localStorage.removeItem("daily-contents-email");
        });
    } catch (err) {}

    // console.log(formDetails);
  }
  async function signup() {
    setOpenSignUp(false);
    // history.push("/SignUp");
  }
  return (
    <>
      {openSignUp ? (
        <>
          <Header
            as="h1"
            color="black"
            textAlign="center"
            style={{ padding: "2vh" }}
          >
            DAILY CONTENT
          </Header>
          <Grid
            textAlign="center"
            style={{ height: "80vh" }}
            verticalAlign="middle"
          >
            <Grid.Column style={{ maxWidth: 450 }}>
              <Header as="h2" color="black" textAlign="center">
                Log-in to your account
              </Header>
              <Form size="large">
                <Segment stacked>
                  <Form.Input
                    fluid
                    icon="user"
                    iconPosition="left"
                    placeholder="E-mail address"
                    onChange={(e) =>
                      setFormDetails({
                        ...formDetails,
                        emailId: e.target.value,
                      })
                    }
                  />
                  <Form.Input
                    fluid
                    icon="lock"
                    iconPosition="left"
                    placeholder="Password"
                    type="password"
                    onChange={(e) =>
                      setFormDetails({
                        ...formDetails,
                        password: e.target.value,
                      })
                    }
                  />

                  <Button
                    color="black"
                    fluid
                    size="large"
                    onClick={submitForm}
                    type="submit"
                  >
                    Login
                  </Button>
                </Segment>
              </Form>
              <Button color="white" fluid size="large" onClick={signup}>
                SignUp
              </Button>
            </Grid.Column>
          </Grid>
        </>
      ) : (
        <SignUp setOpenSignUp={setOpenSignUp} />
      )}
    </>
  );
};

export default LogInPage;
